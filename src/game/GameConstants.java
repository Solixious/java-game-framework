package game;

public class GameConstants {
	public static final String TITLE = "Java Game Framework";
	public static final String VERSION = "0.01";

	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;

	public static final int TILE_WIDTH = 32;
	public static final int TILE_HEIGHT = 32;

	public static final int FPS = 60;
	public static final int TILE_COUNT = 256;
}
